<?php
/*
Plugin Name: Add-cars
Plugin URI: https://codex.wordpress.org
Description: Declares a plugin that will create a custom post type displaying adding cars.
Version: 1.0
Author: Jane Shakhova
Author URI: https://codex.wordpress.org
License: JS
*/

class WP_Custom_Type_Car{

    public function __construct() {
        //remove_role('moto');
        //remove_role('manager');

        register_activation_hook( __FILE__, array($this,'wp_new_custom_role') );// Add a custom user role
        add_action( 'init',array($this,'create_add_cars' ) );
        add_action( 'init', array($this, 'create_my_taxonomies'),0 );
        add_shortcode('add_car_form', array($this, 'display_car_shortcode'));
        add_shortcode('cars_list', array($this, 'display_car_posts'));
        add_action('init',array( $this, 'wp_new_cap') );
        add_action('admin_menu', array( $this, 'add_menu_bar') );
        add_action('admin_menu', array( $this,'register_my_custom_submenu_page_first') );
    }
    public function add_menu_bar(){
        add_menu_page( 'Castom menu', 'Menu', 'manager', 'new_menu', array($this, 'get_the_view_menu'), plugins_url( 'images/star.png', __FILE__ ) , 8.3 );
    }
    public function get_the_view_menu(){
        echo '
            <div class="wrap">
                <h2>' .get_admin_page_title().'</h2>
                <h3>Hello from main menu!</h3>
            </div>';
    }
    public function register_my_custom_submenu_page_first() {
        add_submenu_page('new_menu', 'Submenu1', 'Submenu1', 'manager', 'my-custom-submenu-page1', array($this, 'get_the_view_submenu1'));
        add_submenu_page('new_menu', 'Submenu2', 'Submenu2', 'manager', 'my-custom-submenu-page2', array($this, 'get_the_view_submenu2'));
        add_submenu_page('new_menu', 'Submenu3', 'Submenu3', 'manager', 'my-custom-submenu-page3', array($this, 'get_the_view_submenu3'));
    }
    public function get_the_view_submenu1() {
        echo '<div class="wrap">
                  <h2>Моя страница подменю1</h2>
                  <p>Hello from submenu1!</p>
              </div>';
    }
    public function get_the_view_submenu2() {
        echo '<div class="wrap">
                  <h2>Моя страница подменю2</h2>
                  <p>Hello from submenu2!</p>
              </div>';
    }
    public function get_the_view_submenu3() {
        echo '<div class="wrap">
                  <h2>Моя страница подменю3</h2>
                  <p>Hello from submenu3!</p>
              </div>';
    }
    public function wp_new_cap(){
        $role = get_role( 'manager' );
        $role->add_cap( 'add_cars' );
    }

    public function wp_new_custom_role() {
        add_role( 'manager', __(
                'Manager' ),
            array(
                'read' => true, // true allows this capability
                'edit_posts' => true, // Allows user to edit their own posts
                'edit_others_posts' => false, // Allows user to edit others posts not just their own
                'create_posts' => true, // Allows user to create new posts
                'manage_categories' => true, // Allows user to manage post categories
                'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
                'edit_themes' => false, // false denies this capability. User can’t edit your theme
                'install_plugins' => false, // User cant add new plugins
                'update_plugin' => false, // User can’t update any plugins
                'update_core' => false, // user cant perform core updates
                'moderate_comments' => false,
                'add_cars' => true
            )
        );
        add_role( 'moto', __(
                'Moto' ),
            array(
                'read' => true, // true allows this capability
                'edit_posts' => false, // Allows user to edit their own posts
                'edit_others_posts' => false, // Allows user to edit others posts not just their own
                'create_posts' => false, // Allows user to create new posts
                'edit_post' => false, // Allows user to create new posts
                'manage_categories' => false, // Allows user to manage post categories
                'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
                'edit_themes' => false, // false denies this capability. User can’t edit your theme
                'install_plugins' => false, // User cant add new plugins
                'update_plugin' => false, // User can’t update any plugins
                'update_core' => false, // user cant perform core updates

            )
        );
    }

    public function create_add_cars() {
        $show_type_in_menu = true;
        if( current_user_can('manager') || current_user_can('moto') ){
            $show_type_in_menu = false;
        }else {
            $show_type_in_menu = true;
        }

        register_post_type( 'cars',
            array(
                'labels' => array(
                    'name' => 'Cars',
                    'singular_name' => 'Cars',
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New Car',
                    'edit' => 'Edit',
                    'edit_item' => 'Edit Car',
                    'new_item' => 'New Car',
                    'view' => 'View',
                    'view_item' => 'View Car',
                    'search_items' => 'Search Car',
                    'not_found' => 'No Car found',
                    'not_found_in_trash' => 'No Car found in Trash',
                    'parent' => 'Parent Car'
                ),
                'public' => true,
                'menu_position' => 5,
                'supports' => array("title","editor"),
                'taxonomies' => array( 'cars_model_car' ),
                'menu_icon' => plugins_url( 'images/car.png', __FILE__ ),
                'has_archive' => true,
                'show_in_menu'=> $show_type_in_menu
            )
        );
    }
     public function create_my_taxonomies() {
        register_taxonomy(
            'cars_model_car',
            'cars',
            array(
                'labels' => array(
                    'name' => 'Car model',
                    'add_new_item' => 'Add New Car Model',
                    'new_item_name' => "New Type Car Model"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true
            )
        );
    }

    public function display_car_shortcode(  ) {
        if ( is_user_logged_in() && current_user_can('add_cars') ) {
            if ( isset($_POST['submit']) ) {
                if ( (!empty($_POST['add_car_name'])) && (!empty($_POST['add_cars_model'])) && (!empty($_POST['add_car_description'])) ) {
                    $add_car_name = $_POST['add_car_name'];
                        $args = array(
                           'post_type'  => 'cars',
                           'title'      => $add_car_name
                        );
                        if(! ($loop = get_posts($args)) ) {
                            $add_car_description = $_POST['add_car_description'];
                            $add_cars_model = $_POST['add_cars_model'];
                            $custom_tax = array(
                                'cars_model_car' => array(
                                    $add_cars_model
                                )
                            );
                            $my_post = array(
                                'post_title' => $add_car_name,
                                'post_content' => $add_car_description,
                                'post_status' => 'publish',
                                'post_author' => get_current_user_id(),
                                'post_type' => 'cars',
                                'tax_input' => $custom_tax,
                            );
                            wp_insert_post($my_post);// Insert the post into the database
                        } else { echo '<h3 style="text-align: center; color: red;">This car already exists!</h3>';}
                }else {
                    echo '<h3 style="text-align: center; color: red;">Fill in all the fields!</h3>';
                }
            }
            $html = '
            <form method="post" action="">
            <table>
                <tr>
                    <td style="width: 30%">Car name</td>
                    <td><input type="text" size="80" name="add_car_name" value=""></td>
                </tr>
                <tr>
                    <td>Car model</td>
                    <td>' .
                    wp_dropdown_categories(
                        array(
                            'echo' => '0',
                            'orderby' => 'cars_model_car',
                            'taxonomy' => 'cars_model_car',
                            'name' => 'add_cars_model',
                            'hide_empty' => 0,
                            'show_option_all' => 'Выберите модель',
                        ))
                . '</td>
                </tr>
                <tr>
                    <td>Car description</td>
                    <td><textarea rows="5" cols="45" name="add_car_description"></textarea></td>
                </tr>
                <tr>
                    <td><button type="submit" name="submit" class="send">Send</button></td>
                </tr>
                </table>
            </form>
        ';
            return $html;
        }else{
            return " ";
        }
    }


    public function display_car_posts() {
        if ( is_user_logged_in() && ( current_user_can('moto') || current_user_can('manager') ) ) {
           /*global $wpdb;
             $posts = $wpdb->get_results("SELECT term.name, p.post_title,  p.post_content FROM $wpdb->posts as p, $wpdb->term_relationships as r, $wpdb->term_taxonomy as tt, $wpdb->terms as term " .
                "WHERE p.ID=r.object_id AND r.term_taxonomy_id=tt.term_taxonomy_id  AND tt.term_id=term.term_id AND  p.post_type='cars' AND p.post_author=$author_post");
            var_dump($posts);*/
//            $the_user = get_user_by('login', 'manager');
//            $the_user_id = $the_user->ID;
            $args = array(
                'post_type'      => 'cars',
                "posts_per_page" => 100,
            );
           if (current_user_can('manager')){
                $args ['author'] = get_current_user_id();
            }
            $loop = new WP_Query($args);
            $html = '';
            while ( $loop->have_posts() ) : $loop->the_post();
                $terms = get_the_terms( get_the_ID(), 'cars_model_car' );
                $arr_models = array();
                if (is_array($terms)) {
                    foreach ($terms as $model) {
                        $arr_models[] = $model->name;
                    }
                }else{
                    $arr_models=array('-');
                }
                    $html .= '
                    <div class="entry-content">
                        <strong>Car name: </strong>' . the_title('', '', false) . '<br/>
                        <strong>Car model: </strong> ' .
                        implode(', ', $arr_models)  //change arr to string
                        . '<br/>
                        <strong>Content: </strong>
                        ' . get_the_content() . '<br/>
                    </div>
                 ';
            endwhile;
            return $html;
        }else{
            return " ";
        }
    }
}
$wp_custom_type_car = new WP_Custom_Type_Car();

